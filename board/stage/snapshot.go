package stage

import (
	`gitea.com/whiteboard/core`
	`github.com/storezhang/gox`
	`github.com/storezhang/gox/field`
)

func (s *Snapshot) Size() (size int) {
	for _, user := range s.Users {
		// 必须去除现在上台列表中的讲师或者其它角色，只保证学生
		if core.UserType_STUDENT == user.Type {
			size++
		}
	}

	return
}

func (s *Snapshot) Fields() gox.Fields {
	return gox.Fields{
		field.Int(`count`, len(s.Users)),
		field.Any(`users`, s.Users),
	}
}
