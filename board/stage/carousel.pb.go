// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.14.0
// source: whiteboard/board/stage/carousel.proto

package stage

import (
	core "gitea.com/whiteboard/core"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// 轮播
type Carousel struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 待刷新显示的用户编号列表
	Users []*core.User `protobuf:"bytes,5,rep,name=users,proto3" json:"users,omitempty"`
	// 轮播开启与否
	Switch bool `protobuf:"varint,6,opt,name=switch,proto3" json:"switch,omitempty"`
}

func (x *Carousel) Reset() {
	*x = Carousel{}
	if protoimpl.UnsafeEnabled {
		mi := &file_whiteboard_board_stage_carousel_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Carousel) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Carousel) ProtoMessage() {}

func (x *Carousel) ProtoReflect() protoreflect.Message {
	mi := &file_whiteboard_board_stage_carousel_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Carousel.ProtoReflect.Descriptor instead.
func (*Carousel) Descriptor() ([]byte, []int) {
	return file_whiteboard_board_stage_carousel_proto_rawDescGZIP(), []int{0}
}

func (x *Carousel) GetUsers() []*core.User {
	if x != nil {
		return x.Users
	}
	return nil
}

func (x *Carousel) GetSwitch() bool {
	if x != nil {
		return x.Switch
	}
	return false
}

var File_whiteboard_board_stage_carousel_proto protoreflect.FileDescriptor

var file_whiteboard_board_stage_carousel_proto_rawDesc = []byte{
	0x0a, 0x25, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x62, 0x6f, 0x61,
	0x72, 0x64, 0x2f, 0x73, 0x74, 0x61, 0x67, 0x65, 0x2f, 0x63, 0x61, 0x72, 0x6f, 0x75, 0x73, 0x65,
	0x6c, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x16, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f,
	0x61, 0x72, 0x64, 0x2e, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x73, 0x74, 0x61, 0x67, 0x65, 0x1a,
	0x1a, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x63, 0x6f, 0x72, 0x65,
	0x2f, 0x75, 0x73, 0x65, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x4f, 0x0a, 0x08, 0x43,
	0x61, 0x72, 0x6f, 0x75, 0x73, 0x65, 0x6c, 0x12, 0x2b, 0x0a, 0x05, 0x75, 0x73, 0x65, 0x72, 0x73,
	0x18, 0x05, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x15, 0x2e, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f,
	0x61, 0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x55, 0x73, 0x65, 0x72, 0x52, 0x05, 0x75,
	0x73, 0x65, 0x72, 0x73, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x77, 0x69, 0x74, 0x63, 0x68, 0x18, 0x06,
	0x20, 0x01, 0x28, 0x08, 0x52, 0x06, 0x73, 0x77, 0x69, 0x74, 0x63, 0x68, 0x42, 0x56, 0x0a, 0x22,
	0x63, 0x6f, 0x6d, 0x2e, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x6d,
	0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2e, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x73, 0x74, 0x61,
	0x67, 0x65, 0x50, 0x01, 0x5a, 0x2e, 0x67, 0x69, 0x74, 0x65, 0x61, 0x2e, 0x63, 0x6f, 0x6d, 0x2f,
	0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x61,
	0x67, 0x65, 0x2f, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x73, 0x74, 0x61, 0x67, 0x65, 0x3b, 0x73,
	0x74, 0x61, 0x67, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_whiteboard_board_stage_carousel_proto_rawDescOnce sync.Once
	file_whiteboard_board_stage_carousel_proto_rawDescData = file_whiteboard_board_stage_carousel_proto_rawDesc
)

func file_whiteboard_board_stage_carousel_proto_rawDescGZIP() []byte {
	file_whiteboard_board_stage_carousel_proto_rawDescOnce.Do(func() {
		file_whiteboard_board_stage_carousel_proto_rawDescData = protoimpl.X.CompressGZIP(file_whiteboard_board_stage_carousel_proto_rawDescData)
	})
	return file_whiteboard_board_stage_carousel_proto_rawDescData
}

var file_whiteboard_board_stage_carousel_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_whiteboard_board_stage_carousel_proto_goTypes = []interface{}{
	(*Carousel)(nil),  // 0: whiteboard.board.stage.Carousel
	(*core.User)(nil), // 1: whiteboard.core.User
}
var file_whiteboard_board_stage_carousel_proto_depIdxs = []int32{
	1, // 0: whiteboard.board.stage.Carousel.users:type_name -> whiteboard.core.User
	1, // [1:1] is the sub-list for method output_type
	1, // [1:1] is the sub-list for method input_type
	1, // [1:1] is the sub-list for extension type_name
	1, // [1:1] is the sub-list for extension extendee
	0, // [0:1] is the sub-list for field type_name
}

func init() { file_whiteboard_board_stage_carousel_proto_init() }
func file_whiteboard_board_stage_carousel_proto_init() {
	if File_whiteboard_board_stage_carousel_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_whiteboard_board_stage_carousel_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Carousel); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_whiteboard_board_stage_carousel_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_whiteboard_board_stage_carousel_proto_goTypes,
		DependencyIndexes: file_whiteboard_board_stage_carousel_proto_depIdxs,
		MessageInfos:      file_whiteboard_board_stage_carousel_proto_msgTypes,
	}.Build()
	File_whiteboard_board_stage_carousel_proto = out.File
	file_whiteboard_board_stage_carousel_proto_rawDesc = nil
	file_whiteboard_board_stage_carousel_proto_goTypes = nil
	file_whiteboard_board_stage_carousel_proto_depIdxs = nil
}
