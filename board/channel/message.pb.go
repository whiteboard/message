// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.14.0
// source: whiteboard/board/channel/message.proto

package channel

import (
	core "gitea.com/whiteboard/core"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// 发送消息
// 消息编号的作用是
// 在做Recall这类功能时，必须要根据编号去确定究竟是要取消哪条消息
// 应该根据Type去解析内容，比如
// 普通消息，直接显示
// 语音消息，内容应该是一个URL，把URL对应的文件下载回来，显示成类似微信的声音样式
// 视频消息，内容应该是一个URL，把URL对应的文件下载回来，显示成类似微信的视频样式
type Message struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 编号
	Id string `protobuf:"bytes,3,opt,name=id,proto3" json:"id,omitempty"`
	// 接收者
	// 发送者使用最顶层的Operator
	To *core.User `protobuf:"bytes,5,opt,name=to,proto3" json:"to,omitempty"`
	// 聊天内容
	Content string `protobuf:"bytes,6,opt,name=content,proto3" json:"content,omitempty"`
	// 类型
	Type core.MessageType `protobuf:"varint,7,opt,name=type,proto3,enum=whiteboard.core.MessageType" json:"type,omitempty"`
}

func (x *Message) Reset() {
	*x = Message{}
	if protoimpl.UnsafeEnabled {
		mi := &file_whiteboard_board_channel_message_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Message) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Message) ProtoMessage() {}

func (x *Message) ProtoReflect() protoreflect.Message {
	mi := &file_whiteboard_board_channel_message_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Message.ProtoReflect.Descriptor instead.
func (*Message) Descriptor() ([]byte, []int) {
	return file_whiteboard_board_channel_message_proto_rawDescGZIP(), []int{0}
}

func (x *Message) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Message) GetTo() *core.User {
	if x != nil {
		return x.To
	}
	return nil
}

func (x *Message) GetContent() string {
	if x != nil {
		return x.Content
	}
	return ""
}

func (x *Message) GetType() core.MessageType {
	if x != nil {
		return x.Type
	}
	return core.MessageType_MESSAGE_TYPE_UNSPECIFIED
}

var File_whiteboard_board_channel_message_proto protoreflect.FileDescriptor

var file_whiteboard_board_channel_message_proto_rawDesc = []byte{
	0x0a, 0x26, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x62, 0x6f, 0x61,
	0x72, 0x64, 0x2f, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x61,
	0x67, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x18, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62,
	0x6f, 0x61, 0x72, 0x64, 0x2e, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x63, 0x68, 0x61, 0x6e, 0x6e,
	0x65, 0x6c, 0x1a, 0x1a, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x63,
	0x6f, 0x72, 0x65, 0x2f, 0x75, 0x73, 0x65, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x22,
	0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x63, 0x6f, 0x72, 0x65, 0x2f,
	0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x22, 0x8c, 0x01, 0x0a, 0x07, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x12, 0x0e,
	0x0a, 0x02, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x25,
	0x0a, 0x02, 0x74, 0x6f, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x15, 0x2e, 0x77, 0x68, 0x69,
	0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x2e, 0x55, 0x73, 0x65,
	0x72, 0x52, 0x02, 0x74, 0x6f, 0x12, 0x18, 0x0a, 0x07, 0x63, 0x6f, 0x6e, 0x74, 0x65, 0x6e, 0x74,
	0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x63, 0x6f, 0x6e, 0x74, 0x65, 0x6e, 0x74, 0x12,
	0x30, 0x0a, 0x04, 0x74, 0x79, 0x70, 0x65, 0x18, 0x07, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x1c, 0x2e,
	0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x63, 0x6f, 0x72, 0x65, 0x2e,
	0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x54, 0x79, 0x70, 0x65, 0x52, 0x04, 0x74, 0x79, 0x70,
	0x65, 0x42, 0x5c, 0x0a, 0x24, 0x63, 0x6f, 0x6d, 0x2e, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f,
	0x61, 0x72, 0x64, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2e, 0x62, 0x6f, 0x61, 0x72,
	0x64, 0x2e, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x50, 0x01, 0x5a, 0x32, 0x67, 0x69, 0x74,
	0x65, 0x61, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72,
	0x64, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2f, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f,
	0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x3b, 0x63, 0x68, 0x61, 0x6e, 0x6e, 0x65, 0x6c, 0x62,
	0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_whiteboard_board_channel_message_proto_rawDescOnce sync.Once
	file_whiteboard_board_channel_message_proto_rawDescData = file_whiteboard_board_channel_message_proto_rawDesc
)

func file_whiteboard_board_channel_message_proto_rawDescGZIP() []byte {
	file_whiteboard_board_channel_message_proto_rawDescOnce.Do(func() {
		file_whiteboard_board_channel_message_proto_rawDescData = protoimpl.X.CompressGZIP(file_whiteboard_board_channel_message_proto_rawDescData)
	})
	return file_whiteboard_board_channel_message_proto_rawDescData
}

var file_whiteboard_board_channel_message_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_whiteboard_board_channel_message_proto_goTypes = []interface{}{
	(*Message)(nil),       // 0: whiteboard.board.channel.Message
	(*core.User)(nil),     // 1: whiteboard.core.User
	(core.MessageType)(0), // 2: whiteboard.core.MessageType
}
var file_whiteboard_board_channel_message_proto_depIdxs = []int32{
	1, // 0: whiteboard.board.channel.Message.to:type_name -> whiteboard.core.User
	2, // 1: whiteboard.board.channel.Message.type:type_name -> whiteboard.core.MessageType
	2, // [2:2] is the sub-list for method output_type
	2, // [2:2] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_whiteboard_board_channel_message_proto_init() }
func file_whiteboard_board_channel_message_proto_init() {
	if File_whiteboard_board_channel_message_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_whiteboard_board_channel_message_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Message); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_whiteboard_board_channel_message_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_whiteboard_board_channel_message_proto_goTypes,
		DependencyIndexes: file_whiteboard_board_channel_message_proto_depIdxs,
		MessageInfos:      file_whiteboard_board_channel_message_proto_msgTypes,
	}.Build()
	File_whiteboard_board_channel_message_proto = out.File
	file_whiteboard_board_channel_message_proto_rawDesc = nil
	file_whiteboard_board_channel_message_proto_goTypes = nil
	file_whiteboard_board_channel_message_proto_depIdxs = nil
}
