// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.14.0
// source: whiteboard/board/window/scroll.proto

package window

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// 滚动
type Scroll struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 滚动比例
	Ratio float32 `protobuf:"fixed32,5,opt,name=ratio,proto3" json:"ratio,omitempty"`
}

func (x *Scroll) Reset() {
	*x = Scroll{}
	if protoimpl.UnsafeEnabled {
		mi := &file_whiteboard_board_window_scroll_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Scroll) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Scroll) ProtoMessage() {}

func (x *Scroll) ProtoReflect() protoreflect.Message {
	mi := &file_whiteboard_board_window_scroll_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Scroll.ProtoReflect.Descriptor instead.
func (*Scroll) Descriptor() ([]byte, []int) {
	return file_whiteboard_board_window_scroll_proto_rawDescGZIP(), []int{0}
}

func (x *Scroll) GetRatio() float32 {
	if x != nil {
		return x.Ratio
	}
	return 0
}

var File_whiteboard_board_window_scroll_proto protoreflect.FileDescriptor

var file_whiteboard_board_window_scroll_proto_rawDesc = []byte{
	0x0a, 0x24, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x62, 0x6f, 0x61,
	0x72, 0x64, 0x2f, 0x77, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x2f, 0x73, 0x63, 0x72, 0x6f, 0x6c, 0x6c,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x17, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61,
	0x72, 0x64, 0x2e, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e, 0x77, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x22,
	0x1e, 0x0a, 0x06, 0x53, 0x63, 0x72, 0x6f, 0x6c, 0x6c, 0x12, 0x14, 0x0a, 0x05, 0x72, 0x61, 0x74,
	0x69, 0x6f, 0x18, 0x05, 0x20, 0x01, 0x28, 0x02, 0x52, 0x05, 0x72, 0x61, 0x74, 0x69, 0x6f, 0x42,
	0x59, 0x0a, 0x23, 0x63, 0x6f, 0x6d, 0x2e, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72,
	0x64, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2e, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2e,
	0x77, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x50, 0x01, 0x5a, 0x30, 0x67, 0x69, 0x74, 0x65, 0x61, 0x2e,
	0x63, 0x6f, 0x6d, 0x2f, 0x77, 0x68, 0x69, 0x74, 0x65, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x6d,
	0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x2f, 0x62, 0x6f, 0x61, 0x72, 0x64, 0x2f, 0x77, 0x69, 0x6e,
	0x64, 0x6f, 0x77, 0x3b, 0x77, 0x69, 0x6e, 0x64, 0x6f, 0x77, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x33,
}

var (
	file_whiteboard_board_window_scroll_proto_rawDescOnce sync.Once
	file_whiteboard_board_window_scroll_proto_rawDescData = file_whiteboard_board_window_scroll_proto_rawDesc
)

func file_whiteboard_board_window_scroll_proto_rawDescGZIP() []byte {
	file_whiteboard_board_window_scroll_proto_rawDescOnce.Do(func() {
		file_whiteboard_board_window_scroll_proto_rawDescData = protoimpl.X.CompressGZIP(file_whiteboard_board_window_scroll_proto_rawDescData)
	})
	return file_whiteboard_board_window_scroll_proto_rawDescData
}

var file_whiteboard_board_window_scroll_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_whiteboard_board_window_scroll_proto_goTypes = []interface{}{
	(*Scroll)(nil), // 0: whiteboard.board.window.Scroll
}
var file_whiteboard_board_window_scroll_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_whiteboard_board_window_scroll_proto_init() }
func file_whiteboard_board_window_scroll_proto_init() {
	if File_whiteboard_board_window_scroll_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_whiteboard_board_window_scroll_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Scroll); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_whiteboard_board_window_scroll_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_whiteboard_board_window_scroll_proto_goTypes,
		DependencyIndexes: file_whiteboard_board_window_scroll_proto_depIdxs,
		MessageInfos:      file_whiteboard_board_window_scroll_proto_msgTypes,
	}.Build()
	File_whiteboard_board_window_scroll_proto = out.File
	file_whiteboard_board_window_scroll_proto_rawDesc = nil
	file_whiteboard_board_window_scroll_proto_goTypes = nil
	file_whiteboard_board_window_scroll_proto_depIdxs = nil
}
