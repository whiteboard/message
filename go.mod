module gitea.com/whiteboard/message

go 1.17

require (
	gitea.com/whiteboard/core v0.3.9
	github.com/storezhang/gox v1.8.1
)

require (
	github.com/mitchellh/mapstructure v1.3.3 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/text v0.3.0 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
)
